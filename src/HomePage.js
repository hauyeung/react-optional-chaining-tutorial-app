import React, { useState, useEffect } from "react";
import { browse } from "./requests";
import Card from "react-bootstrap/Card";
import "./HomePage.css";

function HomePage() {
  const [initialized, setIntialized] = useState(false);
  const [feed, setFeed] = useState([]);

  const browserFeed = async () => {
    const response = await browse();
    setFeed(response.data.near_earth_objects);
    setIntialized(true);
  };

  useEffect(() => {
    if (!initialized) {
      browserFeed();
    }
  });
  return (
    <div className="HomePage">
      <h1 className='center'>Asteroids Close to Earth</h1>
      <br />
      {feed?.map(f => {
        return (
          <Card style={{ width: "90vw", margin: "0 auto" }}>
            <Card.Body>
              <Card.Title>{f?.name}</Card.Title>
              <div>
                {f?.close_approach_data?.length > 0 ? (
                  <div>
                    Close Approach Date:
                    {f?.close_approach_data?.map(d => {
                      return <p>{d?.close_approach_date_full}</p>;
                    })}
                  </div>
                ) : null}
                <p>
                  Minimum Estimated Diameter:{" "}
                  {f?.estimated_diameter?.kilometers?.estimated_diameter_min} km
                </p>
                <p>
                  Maximum Estimated Diameter:{" "}
                  {f?.estimated_diameter?.kilometers?.estimated_diameter_max} km
                </p>
              </div>
            </Card.Body>
          </Card>
        );
      })}
    </div>
  );
}

export default HomePage;
