import React from "react";
import { Router, Route } from "react-router-dom";
import HomePage from "./HomePage";
import AsteroidsSearchPage from "./AsteroidsSearchPage";
import { createBrowserHistory as createHistory } from "history";
import "./App.css";
import TopBar from "./TopBar";
const history = createHistory();

function App() {
  return (
    <div className="App">
      <Router history={history}>
        <TopBar />
        <Route path="/" exact component={HomePage} />
        <Route path="/search" exact component={AsteroidsSearchPage} />
      </Router>
    </div>
  );
}

export default App;
