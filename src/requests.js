const APIURL = "https://api.nasa.gov/neo/rest/v1";
const axios = require("axios");

export const searchFeed = data =>
  axios.get(
    `${APIURL}/feed?start_date=${data.startDate}&end_date=${data.endDate}&api_key=${process.env.REACT_APP_APIKEY}`
  );

export const browse = () =>
  axios.get(`${APIURL}/neo/browse?api_key=${process.env.REACT_APP_APIKEY}`);
