import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import * as yup from "yup";
import Card from "react-bootstrap/Card";
import "./AsteroidsSearchPage.css";
import { searchFeed } from "./requests";

const schema = yup.object({
  startDate: yup
    .string()
    .required("Start date is required")
    .matches(
      /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/,
      "Invalid start date"
    ),
  endDate: yup
    .string()
    .required("End date is required")
    .matches(
      /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/,
      "Invalid end date"
    ),
});

function AsteroidsSearchPage() {
  const [feed, setFeed] = useState({});
  const [error, setError] = useState("");

  const handleSubmit = async evt => {
    const isValid = await schema.validate(evt);
    if (!isValid) {
      return;
    }
    try {
      const response = await searchFeed(evt);
      setFeed(response.data.near_earth_objects);
    } catch (ex) {
      alert(ex?.response?.data?.error_message);
    }
  };

  return (
    <div className="AsteroidsSearchPage">
      <h1 className="center">Search Asteroids</h1>
      <Formik validationSchema={schema} onSubmit={handleSubmit}>
        {({
          handleSubmit,
          handleChange,
          handleBlur,
          values,
          touched,
          isInvalid,
          errors,
        }) => (
          <Form noValidate onSubmit={handleSubmit}>
            <Form.Row>
              <Form.Group as={Col} md="12" controlId="startDate">
                <Form.Label>Start Date</Form.Label>
                <Form.Control
                  type="text"
                  name="startDate"
                  placeholder="YYYY-MM-DD"
                  value={values.startDate || ""}
                  onChange={handleChange}
                  isInvalid={touched.startDate && errors.startDate}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.startDate}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="endDate">
                <Form.Label>End Date</Form.Label>
                <Form.Control
                  type="text"
                  name="endDate"
                  placeholder="YYYY-MM-DD"
                  value={values.endDate || ""}
                  onChange={handleChange}
                  isInvalid={touched.startDate && errors.endDate}
                />

                <Form.Control.Feedback type="invalid">
                  {errors.endDate}
                </Form.Control.Feedback>
              </Form.Group>
            </Form.Row>
            <Button type="submit" style={{ marginRight: "10px" }}>
              Search
            </Button>
          </Form>
        )}
      </Formik>
      {Object.keys(feed)?.map(key => {
        return (
          <Card style={{ width: "90vw", margin: "0 auto" }}>
            <Card.Body>
              <Card.Title>{key}</Card.Title>
              {feed?.[key]?.length > 0
                ? feed?.[key]?.map(f => {
                    return (
                      <div style={{ paddingBottom: "10px" }}>
                        {f?.close_approach_data?.length > 0 ? (
                          <div>
                            <b>
                              Close Approach Date:
                              {f?.close_approach_data?.map(d => {
                                return <p>{d?.close_approach_date_full}</p>;
                              })}
                            </b>
                          </div>
                        ) : null}
                        <div>
                          Minimum Estimated Diameter:{" "}
                          {
                            f?.estimated_diameter?.kilometers
                              ?.estimated_diameter_min
                          }{" "}
                          km
                        </div>
                        <div>
                          Maximum Estimated Diameter:{" "}
                          {
                            f?.estimated_diameter?.kilometers
                              ?.estimated_diameter_max
                          }{" "}
                          km
                          <br />
                        </div>
                      </div>
                    );
                  })
                : null}
            </Card.Body>
          </Card>
        );
      })}
    </div>
  );
}

export default AsteroidsSearchPage;
